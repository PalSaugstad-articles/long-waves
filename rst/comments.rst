---------------------------

========
Comments
========

---------------------------

.. topic:: (2019-06-15 12:30):

    This was my entry for the competition
    `What’s the 8th Way to Think Like a 21st Century Economist?
    <https://www.kateraworth.com/8thwaycompetition/>`_
    by
    `Rethinking Economics
    <http://www.rethinkeconomics.org/>`_
    and
    `Doughnut Economics
    <https://www.kateraworth.com/doughnut/>`_.

    My entry can be found
    `here
    <https://www.7vortex.com/ecosystems/d8b41fd2-886d-4191-98e1-8f2bb900fba5/view>`_
    where it is one of eight contributions which are connected to the 'Long Term' bubble.
    (See more information about the competition
    `here
    <https://www.kateraworth.com/2019/06/06/hive-mind-whats-the-8th-way-to-think-like-a-21st-century-economist/>`_)
