The economist of the 21st century must be able to explain the Long Waves and suggest how to even them out.

Kondratiev and others have studied long economic waves already, but I don't think we ever will be have a final answer.
There are so many different activities to consider when talking about waves.

I think economists should put technological issues in the background
and instead focus on democracy, competitiveness, conflicts, crisis and war, as one sort of wave phenomenon.

Instead of building a beautiful harmonious economic theory where such such things as economic crisis and wars are regarded as externalities,
those things should be an integrated part of the long wave economic theory.
If so one would see that there are patterns where economic growth, inequalities, crisis and wars are interlinked and have a certain degree
of repetitiveness about them.

The 'normal' pattern, an about 100 to 200 year long wave, is that inequality increases, then the conflict level increases,
then a big financial crises and a big war 'takes care' of the social limits to growth. After these events we say:
Never Again! Financial measures are implemented, but even more importantly, everybody are busy building up the society again.
A new era of economic growth can start and continue for 100 years more.

Since we are riding along on this ever changing trip through time, this big social experiment that we call 'reality',
it is almost impossible to take a few steps back and study The System.

Nevertheless, it is really important to try to do so.

One area to focus on is analyzing how the financial sector, including the banks, are set up. These sectors are governed by a set of rules that
are possible to alter. The current rules are most likely not optimal.
One indicator is that ever more people are made dependent of a successful financial sector since ever more people are encouraged to stacks up savings
in pension funds.

One effect is that money flows into the money-creation sector (away from the money holding sector) and thus shifts the relationship between the money supply and the debt level to the worse.
Another effect is that everybody will loose (some of) their pension if the pension funds fail, and thus we all become dependent of economic growth.
Economic growth slows down when a society becomes richer (there are empirical evidence, and the Solow Growth Model suggests this),
which in turn increases the probability that the finance sector will collapse.

Well, maybe this analysis isn't entirely correct, but the economist of the 21st century have to figure it out, and explain it in a way that makes
everybody understand what is on stake!

The economist of the 21st century must bring forward convincing suggestions of how to deal with long waves in a peaceful manner.
The economist of the 21st century must make everybody understand that we are all in the same boat and that we have to solve this together.
